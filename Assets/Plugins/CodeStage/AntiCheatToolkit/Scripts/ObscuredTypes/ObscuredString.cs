﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;
namespace CodeStage.AntiCheat.ObscuredTypes
{
    [Serializable]
    public sealed class ObscuredString
    {
        private static string cryptoKey = "4441";
#if UNITY_EDITOR
        public static string cryptoKeyEditor = cryptoKey;
#endif
        [SerializeField]
        private string currentCryptoKey;
        [SerializeField]
        private byte[] hiddenValue;
        [SerializeField]
        private string fakeValue;
        [SerializeField]
        private bool inited;
        private ObscuredString() { }
        private ObscuredString(byte[] value)
        {
            currentCryptoKey = cryptoKey;
            hiddenValue = value;
            fakeValue = null;
            inited = true;
        }
        public static void SetNewCryptoKey(string newKey)
        {
            cryptoKey = newKey;
        }
        public static string EncryptDecrypt(string value)
        {
            return EncryptDecrypt(value, "");
        }
        public static string EncryptDecrypt(string value, string key)
        {
            if (string.IsNullOrEmpty(value))
            {
                return "";
            }
            if (string.IsNullOrEmpty(key))
            {
                key = cryptoKey;
            }
            int keyLength = key.Length;
            int valueLength = value.Length;
            char[] result = new char[valueLength];
            for (int i = 0; i < valueLength; i++)
            {
                result[i] = (char)(value[i] ^ key[i % keyLength]);
            }
            return new string(result);
        }
        public void ApplyNewCryptoKey()
        {
            if (currentCryptoKey != cryptoKey)
            {
                hiddenValue = InternalEncrypt(InternalDecrypt());
                currentCryptoKey = cryptoKey;
            }
        }
        public void RandomizeCryptoKey()
        {
            string decrypted = InternalDecrypt();
            currentCryptoKey = Random.Range(int.MinValue, int.MaxValue).ToString();
            hiddenValue = InternalEncrypt(decrypted, currentCryptoKey);
        }
        public string GetEncrypted()
        {
            ApplyNewCryptoKey();
            return GetString(hiddenValue);
        }
        public void SetEncrypted(string encrypted)
        {
            inited = true;
            hiddenValue = GetBytes(encrypted);
            if (Detectors.ObscuredCheatingDetector.IsRunning)
            {
                fakeValue = InternalDecrypt();
            }
        }
        public string GetDecrypted()
        {
            return InternalDecrypt();
        }
        private static byte[] InternalEncrypt(string value)
        {
            return InternalEncrypt(value, cryptoKey);
        }
        private static byte[] InternalEncrypt(string value, string key)
        {
            return GetBytes(EncryptDecrypt(value, key));
        }
        private string InternalDecrypt()
        {
            if (!inited)
            {
                currentCryptoKey = cryptoKey;
                hiddenValue = InternalEncrypt("");
                fakeValue = "";
                inited = true;
            }
            string key = currentCryptoKey;
            if (string.IsNullOrEmpty(key))
            {
                key = cryptoKey;
            }
            string result = EncryptDecrypt(GetString(hiddenValue), key);
            if (Detectors.ObscuredCheatingDetector.IsRunning && !string.IsNullOrEmpty(fakeValue) && result != fakeValue)
            {
                Detectors.ObscuredCheatingDetector.Instance.OnCheatingDetected();
            }
            return result;
        }
        #region operators and overrides
        public int Length
        {
            get { return hiddenValue.Length / sizeof(char); }
        }
        public static implicit operator ObscuredString(string value)
        {
            if (value == null)
            {
                return null;
            }
            ObscuredString obscured = new ObscuredString(InternalEncrypt(value));
            if (Detectors.ObscuredCheatingDetector.IsRunning)
            {
                obscured.fakeValue = value;
            }
            return obscured;
        }
        public static implicit operator string(ObscuredString value)
        {
            if (value == null)
            {
                return null;
            }
            return value.InternalDecrypt();
        }
        public override string ToString()
        {
            return InternalDecrypt();
        }
        public static bool operator ==(ObscuredString a, ObscuredString b)
        {
            if (ReferenceEquals(a, b))
            {
                return true;
            }
            if ((object)a == null || (object)b == null)
            {
                return false;
            }
            if (a.currentCryptoKey == b.currentCryptoKey)
            {
                return ArraysEquals(a.hiddenValue, b.hiddenValue);
            }
            return string.Equals(a.InternalDecrypt(), b.InternalDecrypt());
        }
        public static bool operator !=(ObscuredString a, ObscuredString b)
        {
            return !(a == b);
        }
        public override bool Equals(object obj)
        {
            if (!(obj is ObscuredString))
                return false;
            return Equals((ObscuredString)obj);
        }
        public bool Equals(ObscuredString value)
        {
            if (value == null) return false;
            if (currentCryptoKey == value.currentCryptoKey)
            {
                return ArraysEquals(hiddenValue, value.hiddenValue);
            }
            return string.Equals(InternalDecrypt(), value.InternalDecrypt());
        }
        public bool Equals(ObscuredString value, StringComparison comparisonType)
        {
            if (value == null) return false;
            return string.Equals(InternalDecrypt(), value.InternalDecrypt(), comparisonType);
        }
        public override int GetHashCode()
        {
            return InternalDecrypt().GetHashCode();
        }
        #endregion
        private static byte[] GetBytes(string str)
        {
            var bytes = new byte[str.Length * sizeof(char)];
            System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }
        private static string GetString(byte[] bytes)
        {
            char[] chars = new char[bytes.Length / sizeof(char)];
            System.Buffer.BlockCopy(bytes, 0, chars, 0, bytes.Length);
            return new string(chars);
        }
        private static bool ArraysEquals(byte[] a1, byte[] a2)
        {
            if (a1 == a2)
            {
                return true;
            }
            if ((a1 != null) && (a2 != null))
            {
                if (a1.Length != a2.Length)
                {
                    return false;
                }
                for (int i = 0; i < a1.Length; i++)
                {
                    if (a1[i] != a2[i])
                    {
                        return false;
                    }
                }
                return true;
            }
            return false;
        }
    }
}