﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;
namespace CodeStage.AntiCheat.ObscuredTypes
{
    [Serializable]
    public struct ObscuredBool : IEquatable<ObscuredBool>
    {
        private static byte cryptoKey = 215;
#if UNITY_EDITOR
        public static byte cryptoKeyEditor = cryptoKey;
#endif
        [SerializeField]
        private byte currentCryptoKey;
        [SerializeField]
        private int hiddenValue;
        [SerializeField]
        private bool fakeValue;
        [SerializeField]
        private bool fakeValueChanged;
        [SerializeField]
        private bool inited;
        private ObscuredBool(int value)
        {
            currentCryptoKey = cryptoKey;
            hiddenValue = value;
            fakeValue = false;
            fakeValueChanged = false;
            inited = true;
        }
        public static void SetNewCryptoKey(byte newKey)
        {
            cryptoKey = newKey;
        }
        public static int Encrypt(bool value)
        {
            return Encrypt(value, 0);
        }
        public static int Encrypt(bool value, byte key)
        {
            if (key == 0)
            {
                key = cryptoKey;
            }
            int encryptedValue = value ? 213 : 181;
            encryptedValue ^= key;
            return encryptedValue;
        }
        public static bool Decrypt(int value)
        {
            return Decrypt(value, 0);
        }
        public static bool Decrypt(int value, byte key)
        {
            if (key == 0)
            {
                key = cryptoKey;
            }
            value ^= key;
            return value != 181;
        }
        public void ApplyNewCryptoKey()
        {
            if (currentCryptoKey != cryptoKey)
            {
                hiddenValue = Encrypt(InternalDecrypt(), cryptoKey);
                currentCryptoKey = cryptoKey;
            }
        }
        public void RandomizeCryptoKey()
        {
            bool decrypted = InternalDecrypt();
            currentCryptoKey = (byte)Random.Range(1, 150);
            hiddenValue = Encrypt(decrypted, currentCryptoKey);
        }
        public int GetEncrypted()
        {
            ApplyNewCryptoKey();
            return hiddenValue;
        }
        public void SetEncrypted(int encrypted)
        {
            inited = true;
            hiddenValue = encrypted;
            if (Detectors.ObscuredCheatingDetector.IsRunning)
            {
                fakeValue = InternalDecrypt();
                fakeValueChanged = true;
            }
        }
        public bool GetDecrypted()
        {
            return InternalDecrypt();
        }
        private bool InternalDecrypt()
        {
            if (!inited)
            {
                currentCryptoKey = cryptoKey;
                hiddenValue = Encrypt(false);
                fakeValue = false;
                fakeValueChanged = true;
                inited = true;
            }
            int value = hiddenValue;
            value ^= currentCryptoKey;
            bool decrypted = value != 181;
            if (Detectors.ObscuredCheatingDetector.IsRunning && fakeValueChanged && decrypted != fakeValue)
            {
                Detectors.ObscuredCheatingDetector.Instance.OnCheatingDetected();
            }
            return decrypted;
        }
        #region operators, overrides, interface implementations
        public static implicit operator ObscuredBool(bool value)
        {
            ObscuredBool obscured = new ObscuredBool(Encrypt(value));
            if (Detectors.ObscuredCheatingDetector.IsRunning)
            {
                obscured.fakeValue = value;
                obscured.fakeValueChanged = true;
            }
            return obscured;
        }
        public static implicit operator bool(ObscuredBool value)
        {
            return value.InternalDecrypt();
        }
        public override bool Equals(object obj)
        {
            if (!(obj is ObscuredBool))
                return false;
            return Equals((ObscuredBool)obj);
        }
        public bool Equals(ObscuredBool obj)
        {
            if (currentCryptoKey == obj.currentCryptoKey)
            {
                return hiddenValue == obj.hiddenValue;
            }
            return Decrypt(hiddenValue, currentCryptoKey) == Decrypt(obj.hiddenValue, obj.currentCryptoKey);
        }
        public override int GetHashCode()
        {
            return InternalDecrypt().GetHashCode();
        }
        public override string ToString()
        {
            return InternalDecrypt().ToString();
        }
        #endregion
    }
}