#pragma warning disable CS0109 
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;
public partial class Base : MonoBehaviour
{

    public static Transform mainCameraTransform { get { return Camera.main.transform; } }
    public static Camera mainCamera { get { return Camera.main; } }
    internal Transform m_transform;
    public virtual Transform tr { get { return m_transform ? m_transform : (m_transform = base.transform); } }
    private Animator m_Animator;
    public Animator animator { get { return m_Animator ? m_Animator : (m_Animator = GetComponent<Animator>()); } }
    public virtual Vector3 pos { get { return tr.position; } set { tr.position = value; } }//2do replace with localpos
    private Animation m_animation;
    public new Animation animation { get { return m_animation ? m_animation : (m_animation = GetComponent<Animation>()); } }
    private Renderer m_renderer;
    public new Renderer renderer { get { return m_renderer ? m_renderer : (m_renderer = GetComponent<Renderer>()); } }
    private Collider m_collider;
    public new Collider collider { get { return m_collider ? m_collider : (m_collider = GetComponent<Collider>()); } }



    private Rigidbody m_rigidbody;
    public new Rigidbody rigidbody { get { return m_rigidbody ? m_rigidbody : (m_rigidbody = GetComponent<Rigidbody>()); } }
    private AudioSource m_audio;
    public new AudioSource audio { get { return m_audio ? m_audio : (m_audio = GetComponent<AudioSource>()); } }
    private Camera m_camera;
    public new Camera camera { get { return m_camera ? m_camera : (m_camera = GetComponent<Camera>()); } }
    private Light m_light;
    public new Light light { get { return m_light ? m_light : (m_light = GetComponent<Light>()); } }
    private static EventSystem m_EventSystem;
    public static EventSystem _EventSystem { get { return m_EventSystem ? m_EventSystem : (m_EventSystem = FindObjectOfType<EventSystem>()); } }

    public Quaternion rot { get { return tr.rotation; } set { tr.rotation = value; } }
    public static Vector3 ZeroY(Vector3 v, float a = 0)
    {
        v.y *= a;
        return v;
    }
    public static IEnumerator RegisterEndOfFrameUpdate(Action a)
    {
        while (true)
        {
            yield return new WaitForEndOfFrame();
            a();
        }
    }

    public static IEnumerator AddMethod(float seconds, Action act)
    {
        yield return new WaitForSeconds(seconds);
        act();
    }
    public IEnumerator AddMethodCor(float seconds, Func<IEnumerator> act)
    {
        yield return new WaitForSeconds(seconds);
        yield return StartCoroutine(act());
    }
    public static IEnumerator AddMethod(Action act)
    {
        yield return null;
        act();
    }
    public static IEnumerator AddMethod(YieldInstruction y, Action act)
    {
        yield return y;
        act();
    }
    public static IEnumerator AddMethod(Func<bool> y, Action act)
    {
        while (!y())
            yield return null;
        act();
    }
    public static IEnumerator AddLoop(Func<bool> y)
    {
        while (!y())
            yield return new WaitForEndOfFrame();
    }
    public void InvokeRepeating(Action a, float time)
    {
        InvokeRepeating(a.Method.Name, 0, time);
    }
    public static Vector2 mouseDelta { get { return new Vector3(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y")); } }


    //mouseDelta = new Vector3(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));
    public static Vector3 GetMove()
    {
        Vector3 v2 = Vector3.zero;
        if (Input.GetKey(KeyCode.W)) v2 += Vector3.forward;
        if (Input.GetKey(KeyCode.S)) v2 += Vector3.back;
        if (Input.GetKey(KeyCode.A)) v2 += Vector3.left;
        if (Input.GetKey(KeyCode.D)) v2 += Vector3.right;
        return v2.normalized;
    }
  
    protected float clampAngle2(float a)
    {
        if (a < 0) return a + 360;
        if (a > 360) return a - 360;
        return a;
    }
    protected float clampAngle(float a)
    {
        if (a > 180) return a - 360;
        if (a < -180) return a + 360;
        return a;
    }
    public float posx { get { return pos.x; } set { var v = pos; v.x = value; pos = v; } }
    public float posy { get { return pos.y; } set { var v = pos; v.y = value; pos = v; } }
    public float posz { get { return pos.z; } set { var v = pos; v.z = value; pos = v; } }
    public float rotx { get { return rot.eulerAngles.x; } set { var e = rot.eulerAngles; e.x = value; rot = Quaternion.Euler(e); } }
    public float roty { get { return rot.eulerAngles.y; } set { var e = rot.eulerAngles; e.y = value; rot = Quaternion.Euler(e); } }
    public float rotz { get { return rot.eulerAngles.z; } set { var e = rot.eulerAngles; e.z = value; rot = Quaternion.Euler(e); } }
    //public Transform tr { get { return transform; } }
    public Vector3 eulerAngles { get { return tr.eulerAngles; } set { tr.eulerAngles = value; } }
    public Vector3 localEulerAngles { get { return tr.localEulerAngles; } set { tr.localEulerAngles = value; } }
    public Vector3 forward { get { return tr.forward; } set { tr.forward = value; } }
    public Vector3 right { get { return tr.right; } set { tr.right = value; } }
    public Transform parent { get { return tr.parent; } set { tr.parent = value; } }
    public Vector3 scale { get { return tr.localScale; } set { tr.localScale = value; } }
    private static Camera m_CameraMain;
    public static Camera CameraMain { get { return m_CameraMain ? m_CameraMain : (m_CameraMain = Camera.main); } }
    public static Transform CameraMainTransform { get { return CameraMain.transform; } }

    public static bool isPlaying;
    public static bool exiting { get { return !isPlaying; } }
    public static Button.ButtonClickedEvent CreateUnityEvent(UnityAction Action)
    {
        var e = new Button.ButtonClickedEvent();
        e.AddListener(Action);
        return e;
    }
    public static IEnumerable TimeElapsedFixed(float seconds, float offset = 0, int max = 10)
    {
        if (seconds <= 0) throw new Exception("interval too small " + seconds);
        var deltaTime = Time.deltaTime;
        while (deltaTime > seconds && seconds > 0 && max-- > 0)
            yield return deltaTime -= seconds;

        if (TimeElapsed(seconds, offset, deltaTime))
            yield return null;

    }
    public static bool TimeElapsed(float seconds, float offset = 0)
    {
        return TimeElapsed(seconds, offset, Time.deltaTime);
    }
    public static bool TimeElapsed(float seconds, float offset, float deltaTime)
    {
        if (deltaTime > seconds || seconds <= 0) return true;
        var time = Time.time - offset;
        if (time % seconds < (time - deltaTime) % seconds)
            return true;
        return false;
    }
  
    public static StringBuilder sbuilder = new StringBuilder();
    public static StringBuilder sbuilder2 = new StringBuilder();
    public T LogScreen<T>(T s)
    {
        if (Time.smoothDeltaTime == Time.fixedDeltaTime)
            sbuilder2.Append(name + ": " + s + "\r\n");
        else
            sbuilder.Append(name + ": " + s + "\r\n");
        return s;
    }

    public static string Concat2(string seperator, params object[] t)
    {
        var key = ArrayHash(t);
        string s;
        if (!stringcache.TryGetValue(key, out s))
            s = stringcache[key] = string.Join(seperator, t.Select(a => a == null ? string.Empty : a.ToString()).ToArray());
        return s;
    }



    public static string Concat(params object[] t)
    {
        var key = ArrayHash(t);

        string s;
        if (!stringcache.TryGetValue(key, out s))
            s = stringcache[key] = string.Concat(t);
        return s;
    }


    public static string Format(string format, params object[] t)
    {
        var key = ArrayHash(t);
        string s;
        if (!stringcache.TryGetValue(key, out s))
            s = stringcache[key] = string.Format(format, t);

        if (stringcache.Count > 10000)
            stringcache.Remove(stringcache.Keys.First());
        return s;

    }


    static Dictionary<int, string> stringcache = new Dictionary<int, string>();
    public static int GetRandomInt() { return UnityEngine.Random.Range(0, int.MaxValue); }

    public static int ArrayHash(object[] t)
    {
        int key = 123;
        for (int i = 0; i < t.Length; i++)
            if (!Equals(t[i], string.Empty) && t[i] != null)
                key = combineHash(key, combineHash(t[i].GetType().GetHashCode(), t[i].GetHashCode()));
        return key;
    }
    private static int combineHash(int h1, int h2)
    {
        return ((h1 << 5) + h1) ^ h2;
    }
    public static float scrollWhell2 { get { return Input.GetAxis("Mouse ScrollWheel"); } }
    public static int scrollWhell { get { var sc = Input.GetAxis("Mouse ScrollWheel"); return sc > 0 ? 1 : sc < 0 ? -1 : 0; } }
}


