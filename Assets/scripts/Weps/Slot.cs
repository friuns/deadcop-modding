using System;
using CodeStage.AntiCheat.ObscuredTypes;

[Serializable]
public class Slot
{
#if game
    internal Gts curGame;
    [FieldAtr(inherit = true, readOnly = true)]
    public string name = "";
    [FieldAtrStart(inherit = true)]
#endif
    public ObscuredInt price;
    public ObscuredFloat weight;
    public bool enabled = true;
    public bool buyable;
    [FieldAtrEnd()]
   
    internal bool have;
    public Slot(int price = 100, float weight = 0)
    {
        this.weight = weight;
        this.price = price;
    }
#if game
    public Action OnBuy = delegate { };
    public void Sell()
    {
        have = false;
        curGame.money -= price;
    }
    public void Buy()
    {
        have = true;
        curGame.money += price;
        OnBuy();
    }
#endif
}