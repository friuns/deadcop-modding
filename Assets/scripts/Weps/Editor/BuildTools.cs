﻿using System.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Microsoft.Win32;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using gui = UnityEngine.GUILayout;
using egui = UnityEditor.EditorGUILayout;
using Object = UnityEngine.Object;

public class BuildTools : EditorWindow
{

    [MenuItem("Tools/Tools")]
    static void OpenWindow()
    {
        GetWindow<BuildTools>();
    }

    static string output = "";
    static LogType logtype;
    private static void Log(string condition, string stackTrace, LogType type)
    {
        output = condition;
        logtype = type;
    }
    List<bs> selected = new List<bs>();
    public static string m_assetName;
    public static string assetName { get { return m_assetName ?? (m_assetName = EditorPrefs.GetString("assetName", "myassets")); } set { if (value != m_assetName) EditorPrefs.SetString("assetName", m_assetName = value); } }


    protected virtual void OnGUI()
    {
        assetName = egui.TextField("asset name:", assetName);
        

        if (gui.Button("build"))
        {
            List<string> list = new List<string>();
            var prefabsDir = "Assets/prefabs/";
            Directory.CreateDirectory(prefabsDir);
            foreach (var m in selected.Where(a => a != null))
            {
                if (m is GunBase)
                    SetLayer(m, "Hands");

                CheckAsset(m.gameObject);
                var prefab = CreatePrefab(m);
                var guid = AssetDatabase.AssetPathToGUID(AssetDatabase.GetAssetPath(prefab.gameObject)).GetHashCode();
                var gunBasePrefab = prefab as GunBase;
                if (gunBasePrefab != null)
                {
                    var gunBase = m as GunBase;
                    gunBasePrefab.id = guid;
                    gunBasePrefab.handsPrefab = CreatePrefab(gunBase.handsPrefab);
                    gunBasePrefab.gunSkinPrefab = CreatePrefab(gunBase.gunSkinPrefab);
                    gunBasePrefab.weaponPickable = CreatePrefab(gunBase.weaponPickable);
                    gunBasePrefab.bulletPrefab = CreatePrefab(gunBase.bulletPrefab.transform).gameObject;
                }

                var playerSkin = prefab as PlayerSkin;
                if (playerSkin != null)
                {
                    SetLayer(prefab, "Player");
                    playerSkin.id = guid;
                }

                CheckAsset(prefab.gameObject);

                
                list.Add(prefabsDir + m.name + ".prefab");
            }
            if (list.Count == 0 || list.All(a => a == null))
                throw new Exception("please select assets first");

            var build = new AssetBundleBuild() { assetNames = list.ToArray(), assetBundleName = assetName + bs.GetRandomInt() + ".unity3dbundle" };

            var bundnlesDir = "AssetBundles/";
            Directory.CreateDirectory(bundnlesDir);
            BuildPipeline.BuildAssetBundles(bundnlesDir, new[] { build }, BuildAssetBundleOptions.None, BuildTarget.StandaloneWindows);
            PlayerPrefs.SetString("assetPath", Path.GetFullPath(bundnlesDir + build.assetBundleName));

            var key = Registry.CurrentUser.OpenSubKey(@"Software\Unity\UnityEditor\DefaultCompany\", true);
            RegistryUtilities.CopyKey(key, "deadcop", Registry.CurrentUser.OpenSubKey(@"Software\DefaultCompany\", true));

            Debug.Log("build success");

        }


        foreach (var m in FindObjectsOfType<bs>())
        {
            if (m is GunBase || m is PlayerSkin || m is GameRes)
            {

                var toggled = selected.Contains(m);
                egui.BeginHorizontal();

                if (egui.Toggle(m.name, toggled) != toggled)
                {
                    if (toggled)
                        selected.Remove(m);
                    else
                        selected.Add(m);
                }
                if (gui.Button("select"))
                    Selection.activeGameObject = m.gameObject;

                egui.EndHorizontal();
            }
        }
        egui.HelpBox(output, logtype == LogType.Log || string.IsNullOrEmpty(output) ? MessageType.None : MessageType.Error);
    }
    private static void SetLayer(bs m, string LayerName)
    {
        foreach (Transform c in m.GetComponentsInChildren<Transform>(true))
            c.gameObject.layer = LayerMask.NameToLayer(LayerName);
    }
    private T CreatePrefab<T>(T g) where T : Component
    {
        var component = PrefabUtility.CreatePrefab("Assets/prefabs/" + g.name + ".prefab", g.gameObject).GetComponent<T>();
        EditorUtility.SetDirty(component);
        return component;
    }



    private static void CheckAsset(GameObject a)
    {
        if (a == null) throw new Exception("prefab is null");
        foreach (var b in a.GetComponentsInChildren<bs>())
        {
            var playskin = b as PlayerSkin;
            if (playskin)
                CheckArray(playskin.gunPlaceHolder);

            CheckForNull(b, new List<object>());
        }
    }
    void OnEnable()
    {
        Application.logMessageReceived += Log;
    }
    void OnDisable()
    {
        Application.logMessageReceived -= Log;
    }
    static List<PosRot> sp = new List<PosRot>();
    [MenuItem("Tools/Pose/Save Pose")]
    public static void SavePose()
    {
        sp = new List<PosRot>();
        foreach (var t in Selection.activeGameObject.GetComponentsInChildren<Transform>())
            sp.Add(new PosRot() { pos = t.position, rot = t.rotation, tr = t });

    }
    [MenuItem("Tools/Pose/Load Pose")]
    public static void LoadPose()
    {
        foreach (PosRot t in sp)
        {
            t.tr.position = t.pos;
            t.tr.rotation = t.rot;
        }
    }

    public class PosRot
    {
        public Vector3 pos;
        public Quaternion rot;
        public Transform tr;
    }

    [MenuItem("Tools/Clear Folders")]
    public static void ClearFolders()
    {
        list = new List<Dir>();
        Recursive();
        list = list.OrderBy(a => a.level).ToList();
        for (int i = list.Count - 1; i >= 0; i--)
        {
            var dir = list[i];
            if (dir.files.All(a => a.EndsWith(".meta")) || dir.files.Count == 0)
            {
                foreach (var a in dir.files)
                    File.Delete(a);
                try
                {
                    Directory.Delete(dir.name, false);
                    File.Delete(dir.name + ".meta");
                }
                catch (IOException e) { }
            }
        }

    }
    public class Dir
    {
        public string name;
        public List<string> files = new List<string>();
        public int level;

    }
    static List<Dir> list;
    private static void Recursive(string dir = "Assets/", int level = 0)
    {
        var d = new Dir();
        d.level = level;
        d.name = dir;
        list.Add(d);

        foreach (var file in Directory.GetFiles(dir))
            d.files.Add(file);

        foreach (var file in Directory.GetDirectories(dir))
            Recursive(file, level + 1);
    }

    public static void CheckForNull(MonoBehaviour gun, List<object> o)
    {
        foreach (var a in gun.GetType().GetFields(BindingFlags.FlattenHierarchy | BindingFlags.Public | BindingFlags.Instance))
        {
            if (new[] { typeof(MonoBehaviour), typeof(Collider), typeof(Transform) }.Any(b => a.FieldType.IsSubclassOf(b)))
            {
                var v = a.GetValue(gun);
                if (o.Contains(v)) return;
                if (v == null) throw new Exception(gun.name + " " + a.FieldType.Name + "." + a.Name + " not set");
                o.Add(v);
                CheckForNull((MonoBehaviour)v, o);
            }
        }
    }
    public static void CheckArray<T>(IList<T> t)
    {
        if (t.Count == 0 || t.Any(a => a == null))
            throw new Exception("array is empty or null " + StackTraceUtility.ExtractStackTrace());
    }

}

