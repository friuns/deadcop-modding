using System;
using System.Linq;
using UnityEngine;
using System.Collections.Generic;
using Random = UnityEngine.Random;

public class Bomb : bsNetwork<Bomb>
{
    public AudioClip pip;
    public AudioClip explode;
    public AudioClip difuse;
    public AudioClip difuseSay2;
    public AudioClip bombpl2;
    public AudioClip c4armed;
    public AudioClip c4_plant;

    public GameObject detonator;
    internal float bombTime = 45f / 60f;
    public float DifusseStart;
#if game
    public void Start()
    {
        _ObsCamera.PlayOneShot(bombpl2);
        PlayOneShot(c4armed);
        PlayOneShot(c4_plant);
        _Game.CenterText("Bomb has been planted");
        _Game.bombs.Add(this);
    }
    public void OnDestroy()
    {
        if (exiting) return;
        _Game.bombs.Remove(this);
    }
    public bool exploded;
    public bool defused;
    public void Update()
    {
        if (exploded || defused) return;

        UpdateBomb();
        bombTime -= Time.deltaTime / 60f;
        if (TimeElapsed(bombTime > .5f ? 1 : bombTime > .2f ? .5f : .2f))
            PlayOneShot(pip);
        if (IsMine)
        {
            if (bombTime < 0)
                RPCExplode();
        }
    }
    private void UpdateBomb()
    {        
        
        if (_Player.team == TeamEnum.CounterTerrorists &&
            (_Player.pos - pos).magnitude < 1 && (_Player.Input2.GetKey(KeyCode.E) || Android) && Time.time - _Player.HitTime > .5f)
        {
            DifusseStart += Time.deltaTime / 5f;
            _Hud.SetProgress(DifusseStart);
            if (DifusseStart > 1)
                RPCDifuse();

        }
        else
            DifusseStart = 0;
    }

    public void RPCExplode()
    {
        CallRPC(Explode);
        NetworkDestroy();
    }
    [PunRPC]
    private void Explode()
    {
        _ObsCamera.PlayOneShot(explode);
        exploded = true;
        if (_Player != null && (_Player.pos - pos).magnitude < 5)
            _Player.RPCDie();
        Instantiate(detonator, pos, Quaternion.identity);
        _ObsCamera.PlayExplodeAnim();
    }
    public void RPCDifuse()
    {
        _Player.curGame.bombDeffused++;
        CallRPC(Difuse);
        NetworkDestroy();
    }
    [PunRPC]
    public void Difuse()
    {
        _ObsCamera.PlayOneShot(difuseSay2);
        PlayOneShot(difuse);
        _Game.CenterText("Bomb has been defused");
        defused = true;
    }
#endif
}