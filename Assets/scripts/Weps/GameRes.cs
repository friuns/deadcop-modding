using UnityEngine;

public class GameRes : bs
{
    public string skullIcon;
    public string headshotIcon;
    public AudioClip victoryRedTeam;
    public AudioClip victoryBlueTeam;
    public AudioClip drawTeam;
    public AudioClip go;
    public AudioClip headShotAnouncher;
    public AudioClip buySound;
    public AudioClip[] multiKillSounds;
    public GameObject holeDecal;
    public GameObject glassHoleDecal;
    public GameObject BloodDecal;
    public Transform concerneParticle;
    public Transform bloodParticle;
    public AudioClip spraySound;
    public GameObject SprayPrefab;
    public GameObject bigExplosion;
    public Transform explosionGrenade;
    public GameObject ExpDecal;
    public AnimationClip[] camDamage;
    public AnimationClip camExplosion;
}