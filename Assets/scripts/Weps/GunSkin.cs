﻿using System;
using System.Linq;

using UnityEngine;
public class GunSkin : bs
{
    
    public Transform bulletTrace;
  

    public Renderer muzzleFlash;
    public Light muzzleFlashLight;
    internal Renderer[] renderers;
    //public new BoxCollider collider;
    public new GameObject gameObject { get { return base.gameObject; } }
    public void Awake()
    {
        renderers = GetComponentsInChildren<MeshRenderer>().Where(a=>a!=muzzleFlash).ToArray();
    }

}
