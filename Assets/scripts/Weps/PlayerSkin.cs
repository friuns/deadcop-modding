using System;
using System.Collections.Generic;
using System.Linq;
using CodeStage.AntiCheat.ObscuredTypes;

using UnityEngine;

[Serializable]
public class PlayerSkinDict : SerializableDictionary<int, PlayerSkin>
{

}


public class PlayerSkin : bs
{
    [FieldAtrStart]
    public ObscuredFloat q3Speed = 0.62f;
    public ObscuredFloat q3Decline = 0.9f;
    public ObscuredFloat q3minVel = 0.2f;
    public ObscuredInt playerLife = 100;
    public string skinName = "";
    public ObscuredFloat jumpFactor = .5f;
    public ObscuredFloat jumpFactorWhenHoldKey = 1;
#if game
    public SubsetList<Team> teams = new SubsetList<Team>(() => bs._Game.teams);
    internal SubsetList<GunBase> weapons = new SubsetList<GunBase>(() => bs._Game.playerPrefab.guns);
#endif
    [FieldAtrEnd]

    public int id;
    public Transform spine;//obsolete
    public Transform spine2;//obsolete

    public Transform[] gunPlaceHolder;

    public Collider Headg;
    public AudioClip[] hitSound;
    public AudioClip dieSound2;
    public AudioClip headShootSound;

    internal Collider[] colliders;
    internal Rigidbody[] rigidbodies;
    internal SkinnedMeshRenderer[] renderers;
    public AudioClip[] dirtSound2;
    public AudioClip[] fallSound;
    public AudioClip exhaused;
    public AudioClip fireinhole2;
#if game
    public Player pl;
    void Awake()
    {
        var h = Headg != null ? Headg.name : null;
        
        colliders = GetComponentsInChildren<Collider>().Where(a=>a.transform.parent!=transform).OrderBy(a => a.name == h ? 1 : a.tag == "Torso" ? 2 : 3).ToArray(); ;
        renderers = GetComponentsInChildren<SkinnedMeshRenderer>();
        rigidbodies = GetComponentsInChildren<Rigidbody>();
        pl = GetComponentInParent<Player>();
    }

    private void Reset()
    {
        foreach (Transform c in GetComponentsInChildren<Transform>(true))
            c.gameObject.layer = LayerMask.NameToLayer("Player");
    }
    public AnimatorOverrideController animatorOverride { get { return animator.runtimeAnimatorController as AnimatorOverrideController; } }
    //[ContextMenu("fillAnims")]
    //void FillAnims()
    //{
    //    dict.Clear();

    //    var ac = (animator.runtimeAnimatorController as AnimatorOverrideController).runtimeAnimatorController as AnimatorController;
    //    foreach (var layer in ac.layers)
    //        FillAnimations(layer.stateMachine);
    //}
    //private void FillAnimations(AnimatorStateMachine sm)
    //{
    //    foreach (var a in sm.stateMachines)
    //        FillAnimations(a.stateMachine);

    //    foreach (var a in sm.states)
    //    {
    //        if (a.state.motion is AnimationClip)
    //            dict[a.state.name] = (AnimationClip)a.state.motion;
    //    }
    //}
    //public AnimationDict dict = new AnimationDict();
    void Start()
    {
        animator.runtimeAnimatorController = Instantiate(animator.runtimeAnimatorController);
    }
    public void OnBeforeSerialize()
    {
        if (!Application.isPlaying)
            skinName = name;
    }
    void OnAnimatorIK(int layer)
    {
        pl.OnAnimatorIK(layer);
    }
#endif
}

