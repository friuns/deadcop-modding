﻿
using System.Collections.Generic;
using CodeStage.AntiCheat.ObscuredTypes;
using UnityEngine;
using UnityEngine.SocialPlatforms;

interface IAllowEmpty { }
public class AK47 : Weapon { }
public class Weapon : GunBase, IAllowEmpty
{
    [FieldAtrStart()]
    [Header("       settings")]
    public ObscuredFloat RunInaccuraty = 1.6f;
    //public ObscuredFloat shootTimeNonAuto = .2f;
    public ObscuredBool automatic;
    public ObscuredFloat shootInterval = 0.0955f;
    public ObscuredFloat cursorRecovery = 30;
    public ObscuredFloat shootBump = 1.35f;
    public ObscuredInt bulletsPerShoot = 1;
    public ObscuredFloat accuraty = 0.003f;
    public ObscuredFloat staticAccuraty;
    public ObscuredFloat shootCursor = 5;
    public ObscuredFloat shootCursorMax = 30;
    public ObscuredFloat bulletPass = 1;
    public ObscuredFloat Range = 900;
    public ObscuredFloat WallThroughFactor = 1;
    public ObscuredFloat headDamageFactor = 5f;
    public ObscuredFloat staticRunAccuraty = 0.03f;
    public ObscuredFloat bulletSpeedFactor = 1;
    [FieldAtrEnd()]



    public int shootFrame;
#if game
    private float shootTime;
    //private ObscuredFloat shootTimeElapsed;

    private float reloadingTime = -999;
    public override bool isReloading { get { return Time.time - EnableTime < drawTime || Time.time - reloadingTime < reloadTime; } }


    public override void Init()
    {
        base.Init();
        slot.OnBuy = delegate { totalBullets = (bullets = maxBullets) * maxClips; };
    }
    public override void Update()
    {
        base.Update();
        if (!pl.visible) return;
        bool muzzleFLash = (Time.time - shootTime < .03f || Time.renderedFrameCount - shootFrame < 3);
        gunSkin.muzzleFlash.enabled = gunSkin.muzzleFlashLight.enabled = pl.modelVisible && muzzleFLash;
        if (pl.observing)
            Hands.MuzzleFlashLight.enabled = Hands.MuzzleFlash.enabled = muzzleFLash;


        cursorOffset = Mathf.MoveTowards(cursorOffset, 0, Time.deltaTime * cursorRecovery);
        if (IsMine)
        {
            if(!haveGun)
                pl.SelectNextGun();
            if (sniperWeapon && !isReloading)
                RPCSetScope(Input2.GetMouseButtonDown(1));

            if (Input2.GetMouseButtonDown(0))
            {
                if (bullets == 0 && totalBullets == 0)
                    pl.PlayOneShot(dryFire);

                if (!isReloading && bullets <= 0 && totalBullets > 0)
                    CallRPC(Reload);
            }

            var shoot = !isReloading && MouseButton && bullets > 0;
            if (Time.time - lastMouseButtonDown > shootInterval || !shoot)
                RPCSetMouseButton(shoot);
        }

        if (remoteMouseButtonDown && automatic)
            foreach (var _ in TimeElapsedFixed(shootInterval, lastMouseButtonDown))
                Shoot();
    }
    public virtual void Shoot()
    {
        float stref = (pl.StrefAim ? 1 : RunInaccuraty);
        Ray ray = new Ray(pl.hpos - pl.CamRnd.transform.forward * .3f, pl.CamRnd.transform.forward);
        ray.direction += Random.insideUnitSphere * (cursorOffset) * accuraty * stref;
        bullets -= 1;
        pl.CamRnd.localRotation = Quaternion.Euler(pl.CamRnd.localRotation.eulerAngles + (Random.insideUnitSphere + Vector3.left) * shootBump * stref);
        shootFrame = Time.renderedFrameCount;
        shootTime = Time.time;
        cursorOffset = Mathf.Min(cursorOffset + shootCursor, shootCursorMax);
        if (!pl.visibleOrBot) return;
        ShootAnim();
        for (int i = 0; i < bulletsPerShoot; i++)
        {
            var ray1 = new Ray(ray.origin, ray.direction + (staticAccuraty + (pl.StrefAim ? 0 : staticRunAccuraty)) * Random.insideUnitSphere);
            Instantiate(bulletPrefab, ray1.origin, Quaternion.LookRotation(ray.direction)).AddComponent<Bullet>().wep = this;
        }
    }
    public override void RPCReload()
    {
        if (pl.visibleOrBot && !isReloading && bullets != maxBullets && totalBullets > 0)
            CallRPC(Reload);
    }
    [PunRPC]
    public override void Reload()
    {
        prints("Reload");
        reloadingTime = Time.time;
        sniperScope = false;

        if (IsMine)
        {
            StartCoroutine(AddMethod(reloadTime, delegate
            {
                if (this is Shotgun)
                {
                    if (bullets < maxBullets && totalBullets > 0)
                    {
                        bullets += 1;
                        totalBullets -= 1;
                    }
                    if (bullets < maxBullets && totalBullets > 0 && !pl.MouseButton)
                        CallRPC(Reload);
                    else
                        CallRPC(EndReload);
                }
                else
                {
                    var take = Mathf.Min(maxBullets - bullets, totalBullets);
                    bullets += take;
                    totalBullets -= take;
                }
            }));
        }
        PlayHandsAnim(HandsAnimations.reload, .1f);
        pl.Fade(TpsAnim.reload,.1f);
    }
    [PunRPC]
    public void EndReload()
    {
        PlayHandsAnim(HandsAnimations.endReload, .1f);
    }
    public void ShootAnim()
    {
        pl.Fade(TpsAnim.shoot);

        pl.PlayOneShot((_ObsCamera.pos - pos).magnitude < 10 || !distanceShoot ? shootSound2 : distanceShoot);

        if (pl.observing)
        {
            Hands.MuzzleFlash.sharedMaterial = Hands.MuzzleFlashMaterials.Random();
            Hands.capsules.Emit();
            Hands.CrossFade(HandsAnimations.shoot.ToStringE(), .1f, 1, 0);
        }
    }


    [PunRPC]
    public override void SetMouseButton(bool value)
    {
        if (value)
            Shoot();
        base.SetMouseButton(value);
    }
    


#endif
}
