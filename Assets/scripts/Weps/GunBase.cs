﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using CodeStage.AntiCheat.ObscuredTypes;
#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.Animations;
#endif
using UnityEngine;
using Object = UnityEngine.Object;
using kv = System.Collections.Generic.KeyValuePair<UnityEngine.AnimationClip, UnityEngine.AnimationClip>;

[Serializable]
public class GunDict : SerializableDictionary<int, GunBase>
{

}
public class GunBase : bsNetwork<GunBase>, ISerializationCallbackReceiver
{
    [FieldAtrStart]
    [Header("       settings")]
    public string gunName = "";
    public ObscuredInt damage = 26;
    public ObscuredInt defClips;
    public ObscuredInt maxBullets = 30;
    public ObscuredInt maxClips = 2;
    public ObscuredFloat runSpeed = 1;
    public ObscuredFloat reloadTime = 1;
    public ObscuredFloat drawTime = .5f;
    public ObscuredBool sniperWeapon;
    public bool canDrop = true;
    //public TpsAnim tpsAnim = TpsAnim.rifle;
    public Slot slot = new Slot();
    [FieldAtrEnd]

    [Header("       audio")]
    public AudioClip shootSound2;
    public AudioClip distanceShoot;
    public AudioClip[] bulletric;
    public AudioClip zoomSound;
    public AudioClip dryFire;
    public AudioClip SoundExpl2;
    public AudioClip grenadeHit;
    public List<AnimationSounds> animationsSounds = new List<AnimationSounds>();
    [Header("       models")]
    public GunSkin gunSkinPrefab;
    public WeaponPickable weaponPickable;
    public HandsSkin handsPrefab;
    public GameObject bulletPrefab;
    [Header("       animations")]
    public AnimationClip tpsIdle;
    public AnimationClip tpsEnquip;
    public AnimationClip tpsReload;
    public AnimationClip tpsShoot;
    public Vector3 tpsRotationOffset = new Vector3(27, 0, 0);
    [Header("       other")]
    public int id = -1;
    internal HandsSkin Hands;
    internal GunSkin gunSkin;
#if game
    public Input2 Input2 { get { return pl.Input2; } }
    internal ObscuredInt bullets;
    internal ObscuredInt totalBullets;
    internal HitScore hitScore { get { return owner.stats.hitScores[gunName]; } }
    public bool disableGun { get { return !(slot.enabled || pl.playerSkinPrefab.weapons.Any(a => a.id == id)); } }

    
    public ObscuredInt gunNumber = 1;
    public virtual bool haveGun { get { return slot.have && !disableGun && (bullets + totalBullets > 0 || this is IAllowEmpty); }  }

    protected bool remoteMouseButtonDown;
    internal float EnableTime;
    internal float cursorOffset;
    public Player pl;
    public bool MouseButton { get { return pl.MouseButton; } }
    public virtual bool isReloading { get; set; }

    public string weaponIcon;
    internal bool sniperScope;
    //internal int cloneOf=-1;
    public override string ToString()
    {
        return gunName;
    }
    void Awake()
    {
        prints("gunbase awake");
        enabled = false;
        pl = GetComponentInParent<Player>();

    }
    void Start()
    {
        id = pl.gunsDict.GetKeyOf(this);
        weaponPickable.arrayID = id;
        weaponPickable.gunNumber = gunNumber;
    }
    public virtual void Init()
    {
        inited = true;
        LoadSkin();
        slot.curGame = pl.gameStats;
        slot.name = gunName;
        slot.OnBuy = () => { };
    }

    public void LoadSkin()
    {
        var prefab = handsPrefab;
        if (!_ObsCamera.hands.TryGetValue(prefab, out Hands))
        {
            _ObsCamera.hands[prefab] = Hands = _Pool.InstanciateAndParent(prefab, _ObsCamera.handsPlaceholder);
            Hands.MuzzleFlash.enabled = false;
            Hands.gameObject.SetActive2(false);
        }
        gunSkin = _Pool.InstanciateAndParent(gunSkinPrefab, pl.transform); //parent in start?
        gunSkin.gameObject.SetActive(false);

    }
    public virtual void LateUpdate()
    {
        gunSkin.transform.SetPositionAndRotation(pl.skin.gunPlaceHolder[0].position, pl.skin.gunPlaceHolder[0].rotation); ;
    }
    public virtual void Update()
    {

    }

    public virtual void RPCSetMouseButton(bool value)
    {
        if (value != remoteMouseButtonDown)
            CallRPC(SetMouseButton, value);
    }
    public ObscuredFloat lastMouseButtonDown;
    [PunRPC]
    public virtual void SetMouseButton(bool value)
    {
        if (value)
            lastMouseButtonDown = Time.time;
        remoteMouseButtonDown = value;
    }
    public virtual void Reload()
    {
    }
    public void RPCSetScope(bool obj)
    {
        if (obj != sniperScope)
            CallRPC(SetScope, obj);
    }
    [PunRPC]
    public virtual void SetScope(bool obj)
    {
        if (pl.observing)
            pl.PlayOneShot(zoomSound);
        sniperScope = obj;
    }

    //public virtual void Reset() { }
    public virtual void RPCReload() { }
    public new bool enabled { get { return base.enabled; } set { base.enabled = value; } }
    //public void OnEnable()
    //{
    //    OnSelectGun(true);
    //}
    //public void OnDisable()
    //{
    //    OnSelectGun(false);
    //}
    public bool inited;
    public virtual void OnSelectGun(bool b)
    {
        if (base.enabled == b) return;
        enabled = b;
        if (enabled && !inited) Init();
        sniperScope = false;
        gunSkin.gameObject.SetActive(b);
        EnableTime = Time.time;
        if (b)
        {
            var gun = _Game.playerPrefab.gun;
            pl.skin.animatorOverride.ApplyOverrides(new kv[] {
                new kv(gun.tpsReload, tpsReload),
                new kv(gun.tpsShoot, tpsShoot),
                new kv(gun.tpsIdle, tpsIdle),
                new kv(gun.tpsEnquip, tpsEnquip),
            });

            pl.Fade(TpsAnim.draw,.1f);
            //pl.Fade(tpsAnim.ToStringE());//do
        }
    }


    public virtual void reset()
    {
        if (isDebug) defClips *= 100;
        slot.have = defClips > 0;
        if (defClips > 0)
        {
            totalBullets = (defClips - 1) * maxBullets;
            bullets = maxBullets;
        }
    }
    [PunRPC]
    public void PlayHandsAnim(HandsAnimations anim, float trans = 0)
    {
        if (pl.observing)
            Hands.CrossFade(anim.ToStringE(), trans);

        foreach (var a in animationsSounds)
            if (a.anim == anim)
                pl.StartCoroutine(AddMethod(a.time, delegate { pl.PlayOneShot(a.sound); }));
    }



    public void RpcDropWeapon()
    {
        if (!canDrop) return;
        CallRPC(DropWeapon, (int)maxBullets, (int)maxBullets, Vector3.zero, PhotonNetwork.AllocateSceneViewID());

    }
    [PunRPC]
    public void DropWeapon(int bullets, int totalBullets, Vector3 v, int id)
    {
        prints("DropWeapon");
        this.totalBullets = this.bullets = 0;
        weaponPickable.photonView.viewID = id;
        var drop = Instantiate(weaponPickable,gunSkin.pos,gunSkin.rot);
        drop.timedropped = Time.time;
        drop.bullets = bullets;
        drop.totalBullets = totalBullets;
        slot.have = false;
    }

#endif

    public void OnBeforeSerialize()
    {
        if (!Application.isPlaying)
            gunName = name;
    }

    public void OnAfterDeserialize()
    {
    }

}