﻿using System;
using System.Collections;
using System.Linq;
using UnityEngine;
using System.Collections.Generic;
using Random = UnityEngine.Random;
public class C4 : GunBase
{


#if game
    public override void Update()
    {
        base.Update();
        if (IsMine)
            RPCSetMouseButton(haveGun && pl.bombIsNear && MouseButton);

        if (pl.observing)
        {
            
            var planting = pl.bombIsNear && remoteMouseButtonDown;
            Hands.CrossFade(planting ?HandsAnimations.pressbutton.ToStringE() : HandsAnimations.idle.ToStringE() ,.1f);

            if (remoteMouseButtonDown)
            {
                float tm = (Time.time - lastMouseButtonDown) / reloadTime;
                if (tm < 1)
                    _Hud.SetProgress(tm);
                else if (IsMine && pl.ControllerIsGrounded)
                {
                    _Player.curGame.bombPlanted++;
                    PhotonNetwork.InstantiateSceneObject("bomb", pl.pos, Quaternion.identity, PhotonGroup.bomb, null);
                    bullets = 0;
                    pl.LastGun();
                }
            }
        }
    }
#endif
}