﻿using System;
using CodeStage.AntiCheat.ObscuredTypes;
using UnityEngine;


public class WeaponPickable : ItemBase
{
    //public GunBase gun { get { return _Player.gunsDict[arrayID]; } }
    public ObscuredInt bullets;
    public ObscuredInt totalBullets;
    public ObscuredInt gunNumber = 1;
    public int arrayID = -1;
    public float pickTime;
    internal float timedropped;
#if game
    public override void Awake()
    {
        pickTime = Time.time;
        base.Awake();
    }
    public void OnTriggerEnter(Collider other)
    {
        var pl = other.transform.root.GetComponent<Player>();
        if (!pl || !pl.IsMine || Time.time - timedropped < .1f) return;
        if (pl.canTake(gunNumber) && Time.time - pickTime > .5f && (arrayID != pl.c4.id || pl.team == TeamEnum.Terrorists))
        {
            rpcCount++;
            photonView.RPC(Method(Take), PhotonTargets.AllViaServer, pl.viewId, bullets, totalBullets);
            enabled = false;
        }

    }

    [PunRPC]
    public void Take(int viewid, int bullets, int totalBullets)
    {
        var pl = ToObject<Player>(viewid);
        var g = pl.gunsDict[arrayID];
        g.totalBullets = totalBullets;
        g.bullets = bullets;
        g.slot.have = true;
        if (_Loader.loaderPrefs.WeaponAutoSwitch && IsMine)
            pl.RPCSelectGun(arrayID);

        PhotonNetwork.UnAllocateViewID(viewid);
        Destroy(gameObject);

    }

#endif
}