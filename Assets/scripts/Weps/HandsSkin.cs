﻿#pragma warning disable 618
using System;
using System.Collections.Generic;
using UnityEngine;




public class HandsSkin : bs
{
    public new GameObject gameObject { get { return base.gameObject; } }
    
    public ParticleEmitter capsules;
    public Light MuzzleFlashLight;
    public Renderer MuzzleFlash;
    public Material[] MuzzleFlashMaterials;
    public Transform CODgunPivot;
    
    public new Animator animation { get { return base.animator; } }
    public new Animator animator { get { return base.animator; } }
   
#if game
    public void CrossFade(string stateName, float normalizedTransitionDuration, int layer = -1, float normalizedTimeOffset = float.NegativeInfinity, float normalizedTransitionTime = 0)
    {
        prints(stateName);
        animator.CrossFade(stateName, normalizedTransitionDuration, layer, normalizedTimeOffset, normalizedTransitionTime);
    }

    private void Reset()
    {
        foreach (Transform c in GetComponentsInChildren<Transform>(true))
            c.gameObject.layer = LayerMask.NameToLayer("Hands");
    }
#endif
}
public enum HandsAnimations
{
    shoot, reload, endReload, draw,
    throwGrenade, startThrow, pressbutton, idle, cam_idle, cam_run, cam_move

}
[Serializable]
public class AnimationSounds
{
    public HandsAnimations anim;
    public float time;
    public AudioClip sound;
}