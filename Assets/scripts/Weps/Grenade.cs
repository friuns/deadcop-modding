#define Final
using System;
using System.Collections;
using System.Linq;
using UnityEngine;
using System.Collections.Generic;
using Random = UnityEngine.Random;
using CodeStage.AntiCheat.ObscuredTypes;

public class Grenade : GunBase
{

    [FieldAtrStart()]
    public ObscuredFloat startThrowLength = 1;
    public ObscuredFloat throwGrenadeLength = 1;
    public ObscuredFloat Force = 15;
    public ObscuredFloat Up = 3;
    public ObscuredFloat time = 2;
    public ObscuredFloat range = 10;
    [FieldAtrEnd()]

    private ObscuredFloat throwTime;
#if game

    public override void Update()
    {
        base.Update();
        if (!pl.IsMine) return;

        if (throwTime == 0)
        {
            if (pl.MouseButton && haveGun)
            {
                throwTime = Time.time;
                CallRPC(PlayHandsAnim, HandsAnimations.startThrow, .1f);
            }
        }
        else if (Time.time - throwTime > startThrowLength && !pl.MouseButton)
        {
            CallRPC(PlayHandsAnim, HandsAnimations.throwGrenade, .1f);
            StartCoroutine(AddMethod(throwGrenadeLength, delegate
            {
                bullets -= 1;
                CallRPC(Shoot, pos, (transform.forward * Force + transform.up * Up), PhotonNetwork.AllocateViewID());
                StartCoroutine(AddMethod(.5f, delegate
                {
                    throwTime = 0;
                    if (bullets > 0 && !Android)
                        CallRPC(PlayHandsAnim, HandsAnimations.draw, 0f);
                    else
                        pl.LastGun();
                }));
            }));
        }
    }
    [PunRPC]
    void Shoot(Vector3 pos, Vector3 v, int viewId)
    {
        var g = Instantiate(bulletPrefab, pos, Quaternion.LookRotation(v)).AddComponent<GrenadeBullet>();
        g.photonView.viewID = viewId;
        g.gun = this;
        pl.Fade(TpsAnim.shoot);
        pl.PlayOneShot(pl.skin.fireinhole2);
    }
    public override void OnSelectGun(bool b)
    {
        throwTime = 0;
        base.OnSelectGun(b);
    }



    [PunRPC]
    public override void SetMouseButton(bool value)
    {
        base.SetMouseButton(value);
    }
#endif
}